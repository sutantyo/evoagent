package main.evointerface.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by daniels on 9/08/2015.
 */
public class ParameterPanel extends JTabbedPane{

    /**
     * The HashMap containing the JTextField objects
     */
    private HashMap<String,JTextField> textFields;

    /**
     * Create a JTabbedPane (i.e a panel with tabs) consisting of three tabs; General, Agent, and Peptide,
     * with each tab holding a list of parameters in JTextFields (parameters related to its title).
     * The input parameters are three String-to-String HashMaps.
     * In each HashMap, the first String holds the name of the parameter and should be placed in the JLabel
     * while the second String contains the default value for the JTextField.
     *
     * The constructor also creates a HashMap textFields which is going to contain the JTextField objects
     * to be returned later (by the method getParameterValues().
     *
     * @param generalParameters general parameters names and default values
     * @param agentParameters agent parameters names and default values
     * @param peptideParameters peptide parameters names and default values
     */
    public ParameterPanel(LinkedHashMap<String,String> generalParameters,
                          LinkedHashMap<String,String> agentParameters,
                          LinkedHashMap<String,String> peptideParameters){

        textFields = new HashMap<String,JTextField>();

        this.add("General", createParameterPanel(generalParameters));
        this.add("Agent", createParameterPanel(agentParameters));
        this.add("Peptide", createParameterPanel(peptideParameters));
    }

    /**
     * Method to create a panel containing JLabel-JTextField pairs based on the
     * Strings in the HashMap input.
     *
     * @param parameters
     * @return
     */
    private JPanel createParameterPanel(LinkedHashMap<String, String> parameters){
        JPanel panel = new JPanel(new MigLayout("fillx, wrap 2, w 240"));
        for (Map.Entry<String,String> entry : parameters.entrySet()){
            panel.add(new JLabel(entry.getKey()));
            JTextField field = new JTextField(entry.getValue());
            panel.add(field, "span, grow, w 80, align left");

            textFields.put(entry.getKey(), field);
        }
        return panel;
    }


    public HashMap<String,String> getParameterValues()
    {
        HashMap<String,String> values = new HashMap<String,String>();

        for (Map.Entry<String,JTextField> entry : textFields.entrySet()) {
            values.put(entry.getKey(),entry.getValue().getText());
        }
        return values;
    }
}
