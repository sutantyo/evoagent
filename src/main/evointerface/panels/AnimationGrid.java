package main.evointerface.panels;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by daniels on 13/07/2015.
 */

public class AnimationGrid extends JPanel {

    private static int gridHeight;
    private static int gridWidth;
    private static int gridGap;

    private Color[][] cellInfo;

    private Graphics2D canvas;

    public AnimationGrid(int gridHeight, int gridWidth, int gridGap){
        this.setLayout(new MigLayout("align center, fill, wrap 1"));
        setGridSize(gridHeight,gridWidth,gridGap);
        this.gridGap = gridGap;


        cellInfo = new Color[gridHeight][gridWidth];
        for (int i = 0; i < gridHeight; i++)
            for (int j = 0; j < gridWidth; j++)
                cellInfo[i][j] = new Color(128,128,128);
    }

    public AnimationGrid(int gridHeight, int gridWidth){
        this(gridHeight,gridWidth,0);
    }

    @Override
    protected void paintComponent(Graphics g) {

        int panelWidth = this.getParent().getWidth();
        int panelHeight = this.getParent().getHeight();

        int cellWidth = panelWidth/getGridWidth() - gridGap;
        int cellHeight = panelHeight/getGridHeight() - gridGap;
        int xOffset = cellWidth + gridGap;
        int yOffset = cellHeight + gridGap;

        super.paintComponent(g);

        canvas = (Graphics2D)g;
        canvas.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for (int i = 0; i < gridHeight; i++)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                canvas.setColor(getCellColor(i,j));
                canvas.fillRect(j * xOffset, i * yOffset, cellWidth, cellHeight);
            }
        }
        //canvas.fillRect(0, 0, cellWidth, cellHeight);
    }

    public void setGridSize(int gridHeight, int gridWidth, int gridGap) {
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;
        this.gridGap = gridGap;
        cellInfo = new Color[gridHeight][gridWidth];
        for (int i = 0; i < gridHeight; i++)
            for (int j = 0; j < gridWidth; j++)
                cellInfo[i][j] = new Color(128,128,128);
    }

    public int getGridHeight(){
        return this.gridHeight;
    }

    public int getGridWidth(){
        return this.gridWidth;
    }
    public void updateCellInfo(Color[][] newCellInfo){
        cellInfo = new Color[getGridHeight()][getGridWidth()];
        for(int i = 0; i < getGridHeight(); i++)
            for(int j = 0; j < getGridWidth(); j++) {
                cellInfo[i][j] = newCellInfo[i][j];
            }
        repaint();
    }

    public Color getCellColor(int i, int j)
    {
        return cellInfo[i][j];
    }

}
