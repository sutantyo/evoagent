package main.evointerface.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniels on 9/08/2015.
 */
public class InfoPanel extends JTabbedPane {

    private HashMap<String,JTextField> infoFields;
    private JPanel panel;

    private HashMap<String,JLabel> displayedInformation;
    // this is a hashmap that contains reference to the values which are displayed on the info panel


    /**
     * Constructor
     *
     * The constructor accepts a HashMap<String,String> which contains pairs of name-and-value pairs
     * of the data that are going to be displayed.
     * The constructor creates its own internal hashmap <code>displayedInformation</code> for
     * these input values, creates a child panel labelled Info, then call
     * <code>updateInfoPanel(...)</code> to display the data.
     *
     * @param info (required) is a HashMap<String,String> containing the information to be displayed
     *             in the form of string pairs, containing the name of the data and its value.
     * @author Daniel Sutantyo
     */
    public InfoPanel(HashMap<String,String> info) {
        displayedInformation = new HashMap<String, JLabel>();
        this.add("Info",createInfoPanel(info));
    }

    private JPanel createInfoPanel(HashMap<String,String> info){
        panel = new JPanel(new MigLayout("fillx, wrap 2, w 240"));

        for (Map.Entry<String,String> entry : info.entrySet()){
            JLabel key = new JLabel(entry.getKey());
            JLabel val = new JLabel(entry.getValue());
            displayedInformation.put(entry.getKey(),val);
            panel.add(key);
            panel.add(val);
            //infoFields.put(entry.getKey(), field);
        }
        return panel;
    }

    /**
     * Updates the value in displayedInformation to whatever value is passed in by the input
     * parameter.
     *
     * @param info (required) is a hashmap containing the information to be displayed
     *             in the form of string pairs, containing the name of the data and its value.
     */
    public void updateInfoPanel(HashMap<String,String> info){

        for (Map.Entry<String,JLabel> entry : displayedInformation.entrySet()){
            String value =  info.get(entry.getKey());
            if (value != null){
                entry.getValue().setText(value);
            }
        }
        panel.repaint();
    }

}
