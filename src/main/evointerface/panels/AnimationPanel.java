package main.evointerface.panels;

import main.evointerface.InterfaceHelper;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * Created by daniels on 9/08/2015.
 */

/* AnimationPanel is the JFrame which contains the animation of the agents
 *
 */
public class AnimationPanel extends JTabbedPane {

    private AnimationGrid animationGrid;
    private AnimationGrid peptideGrid;
    private AnimationGrid targetGrid;

    private int gridHeight;
    private int gridWidth;
    private int gridGap;

    public AnimationPanel(HashMap<String,String> parameters) {
    }

    public void createAnimationGrids(int gridHeight, int gridWidth){

        animationGrid = new AnimationGrid(gridHeight,gridWidth);
        animationGrid.setLayout(new MigLayout("align center, fill, wrap 1"));

        peptideGrid = new AnimationGrid(gridHeight,gridWidth);
        peptideGrid.setLayout(new MigLayout("align center, fill, wrap 1"));

        targetGrid = new AnimationGrid(gridHeight,gridWidth);
        targetGrid.setLayout(new MigLayout("align center, fill, wrap 1"));

        this.add("Animation",animationGrid);
        this.add("Peptides",peptideGrid);
        this.add("Target",targetGrid);
    }

    public void removeAllGrids(){
        this.removeAll();
    }

    public void removeGrid(String gridTitle){
        int toRemove = indexOfTab(gridTitle);
        if (toRemove != -1){
            this.remove(toRemove);
        }
    }


    public void updateAnimationGrid(Color[][] cellColors, Color[][] peptideColors, Color[][] targetColors){
        animationGrid.updateCellInfo(cellColors);
        peptideGrid.updateCellInfo(peptideColors);
        targetGrid.updateCellInfo(targetColors);
    }

    public void updateTargetGrid(Color [][]targetColors){
        targetGrid.updateCellInfo(targetColors);
    }

    public AnimationGrid getAnimationGrid() {
        return this.animationGrid;
    }
    public AnimationGrid getTargetGrid() {
        return this.targetGrid;
    }
    public AnimationGrid getPeptideGrid() {
        return this.peptideGrid;
    }
}
