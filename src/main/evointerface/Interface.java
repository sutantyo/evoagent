package main.evointerface;

import main.evointerface.panels.AnimationPanel;
import main.evointerface.panels.InfoPanel;
import main.evointerface.panels.ParameterPanel;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by daniels on 12/07/2015.
 */
public class Interface extends JFrame {

    // Interface is a JFrame containing
    // mainPanel (the top level JPanel), which in turns contains
    // the three smaller panels (parameter, animation, and info panel)
    private JPanel mainPanel;

    private ParameterPanel parameterPanel;
    private InfoPanel infoPanel;
    private AnimationPanel animationPanel;
    private JPanel buttonPanel;

    // the start and reset buttons on the mainPanel
    public JButton loadButton;
    public JButton createButton;
    public JButton startButton;
    public JButton stopButton;


    /**
     * Creates a GUI for the EvoAgent simulation.
     *
     * The Interface is a JFrame consisting of three JPanels: ParameterPanel, InfoPanel, and AnimationPanel
     * (each class extends JPanel). The Interface() constructor calls the constructor for each of these panels.
     *
     * The default values for the simulation parameters (e.g. number of agents, grid height and width) are
     * stored as private variables in this class and passed on to the constructor of each panel.
     * The default values are stored in this class for easy modification (e.g. when debugging).
     *
     */
    public Interface()
    {
        /** Create hashmaps containing the starting value for the parameters */
        LinkedHashMap<String,String> SimulationStartingParams = new LinkedHashMap<String,String>();
        LinkedHashMap<String,String> AgentStartingParams = new LinkedHashMap<String,String>();
        LinkedHashMap<String,String> PeptideStartingParams = new LinkedHashMap<String,String>();
        HashMap<String,String> InfoParams = new HashMap<String,String>();

        //SimulationStartingParams.put("Grid Gap",           "0");
        //SimulationStartingParams.put("Grid Height",        "0");
        //SimulationStartingParams.put("Grid Width",         "0");
        SimulationStartingParams.put("Random Seed",       "500");
        SimulationStartingParams.put("Number of Trials",    "10");
        SimulationStartingParams.put("Steps per Trial",     "100");

        //Integer noOfAgent = height*width*2/3;
        AgentStartingParams.put("Number of Agents",        "200");
        AgentStartingParams.put("Max Motility",             "3");
        AgentStartingParams.put("Max Sensitivity",          "8");
        AgentStartingParams.put("Max Colours",              "3");
        AgentStartingParams.put("Max Agent Lifespan",       "10");
        AgentStartingParams.put("Max Survival Rate",       "0.7");
        AgentStartingParams.put("Max Reproduction Rate",       "0.7");
        AgentStartingParams.put("Max Peptides Produced",    "5");

        //Integer noOfPeptides = noOfAgent*2;
        PeptideStartingParams.put("Starting Peptides",      "0");
        PeptideStartingParams.put("Max Diffusion Rate",     "3");
        PeptideStartingParams.put("Max Expiry Rate",        "0.3");
        //PeptideStartingParams.put("Peptide Lifespan",       "10");

        InfoParams.put("Agents",        "0");
        //InfoParams.put("Peptides",      "0");
        InfoParams.put("Fitness",       "0");
        InfoParams.put("Turn",          "0");
        InfoParams.put("Parent Change", "0");


        /** Create the sub panels*/
        animationPanel = new AnimationPanel(SimulationStartingParams);
        parameterPanel = new ParameterPanel(SimulationStartingParams, AgentStartingParams, PeptideStartingParams);
        infoPanel = new InfoPanel(InfoParams);
        buttonPanel = createButtonPanel();

        /** Create the main panel and add the sub panels into it */
        MigLayout parentLayout = new MigLayout("fillx,filly,insets 20, gap 5");
        mainPanel = new JPanel(parentLayout);
        mainPanel.add(parameterPanel, "growy, span 1 1");
        mainPanel.add(animationPanel, "grow, push, span 1 2");
        mainPanel.add(infoPanel, "growy, wrap, span 1 2");
        mainPanel.add(buttonPanel, "growx");


        /** Create the main frame, add the main panel onto it, then display the whole interface */
        this.pack();
        this.getContentPane().add(mainPanel);
        this.setSize(1325,620);
        this.setVisible(true);
    }


    public HashMap<String,String> getInputValues(){
        HashMap<String,String> values = parameterPanel.getParameterValues();
        //values.put("gridHeight", Integer.toString(gridHeight));
        //values.put("gridWidth", Integer.toString(gridWidth));

        return values;
    }

    /*
    public void changeGridSize(int newGridSize)
    {
        if (newGridSize == 0)
            newGridSize = 1;

        gridSize = newGridSize;
        animationPanel.setGridSize(gridSize);
        animationPanel.repaint();
    }
    */

    public void updateInfoPanel(HashMap<String,String> info){
        infoPanel.updateInfoPanel(info);
        infoPanel.repaint();
    }

    public void updateTargetGrid(int[][] targetInfo){
        Color[][] targetColors = new Color[targetInfo.length][targetInfo[0].length];
        for (int i = 0; i < targetInfo.length; i++) {
            for (int j = 0; j < targetInfo[0].length; j++) {
                switch (targetInfo[i][j]){
                    case 0:
                        targetColors[i][j] = Color.lightGray;
                        break;
                    case 1:
                        targetColors[i][j] = Color.red;
                        break;
                    case 2:
                        targetColors[i][j] = Color.white;
                        break;
                    case 3:
                        targetColors[i][j] = Color.blue;
                }
            }
        }

        animationPanel.updateTargetGrid(targetColors);
        animationPanel.repaint();
    }


    public void updateGrid(int[][] cellInfo, int[][] peptideInfo, int[][] targetInfo)
    {
        //animationPanel.getAnimationGrid().setGridSize(gridHeight, gridWidth, gridGap);
        //animationPanel.getPeptideGrid().setGridSize(gridHeight, gridWidth, gridGap);
        Color[][] cellColors = new Color[cellInfo.length][cellInfo[0].length];
        for (int i = 0; i < cellInfo.length; i++) {
            for (int j = 0; j < cellInfo[0].length; j++) {
                switch (cellInfo[i][j]) {
                    case 0:
                        cellColors[i][j] = Color.lightGray;
                        break;
                    case 1:
                        cellColors[i][j] = Color.red;
                        break;
                    case 2:
                        cellColors[i][j] = Color.white;
                        break;
                    case 3:
                        cellColors[i][j] = Color.blue;
                }
            }
        }

        Color[][] peptideColors = new Color[peptideInfo.length][peptideInfo[0].length];
        for (int i = 0; i < peptideInfo.length; i++) {
            for (int j = 0; j < peptideInfo[0].length; j++) {
                switch (peptideInfo[i][j]){
                    case 0:
                        peptideColors[i][j] = Color.white;
                        break;
                    default:
                        peptideColors[i][j] = new Color(0,0,peptideInfo[i][j]%256);

                }
            }
        }
        //animationPanel.getAnimationGrid().updateCellInfo(cellColors);
        //animationPanel.getPeptideGrid().updateCellInfo(cellColors);

        Color[][] targetColors = new Color[targetInfo.length][targetInfo[0].length];
        for (int i = 0; i < targetInfo.length; i++) {
            for (int j = 0; j < targetInfo[0].length; j++) {
                switch (targetInfo[i][j]){
                    case 0:
                        targetColors[i][j] = Color.lightGray;
                        break;
                    case 1:
                        targetColors[i][j] = Color.red;
                        break;
                    case 2:
                        targetColors[i][j] = Color.white;
                        break;
                    case 3:
                        targetColors[i][j] = Color.blue;
                }
            }
        }

        animationPanel.updateAnimationGrid(cellColors, peptideColors, targetColors);
        animationPanel.repaint();

    }

    private JPanel createButtonPanel()
    {
        MigLayout buttonLayout = new MigLayout("fill, wrap 2, w 200, h 200");
        JPanel panel = new JPanel(buttonLayout);

        loadButton = new JButton("Load File");
        panel.add(loadButton, "span, growx, h 50");

        createButton = new JButton("Create Simulation");
        panel.add(createButton, "span, growx, h 50");

        startButton = new JButton("Start");
        startButton.setMnemonic('s');
        panel.add(startButton, "span, split 2, growx, h 50");

        stopButton = new JButton("Stop");
        panel.add(stopButton, "growx, h 50");

        return panel;
    }

    public HashMap<String,String> getParameterValues()
    {
        return parameterPanel.getParameterValues();
    }


    public JTabbedPane getAnimationPanel(){
        return animationPanel;
    }

    public void createAnimationGrids(int gridHeight, int gridWidth){
        animationPanel.createAnimationGrids(gridHeight, gridWidth);
    }




}


