package main.evointerface;

/**
 * Created by daniels on 31/10/2015.
 */
public class InterfaceHelper {


    public static int extract_integer(String s){
        String value = s.replaceAll("\\D","");
        if (value.isEmpty())
            return 0;
        else
            return Integer.parseInt(value);
    }

    public static double extract_double(String s){
        String value = s.replaceAll("\\s","");
        if (value.isEmpty()) {
            return 0;
        }
        else {
            return Double.parseDouble(value);
        }
    }
}
