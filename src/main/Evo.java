package main;

import main.agent.Simulation;
import main.evointerface.Interface;
import main.evointerface.InterfaceHelper;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;


/**
 * Agent-based simulation
 *
 * <p>
 * The class {@code Evo} is the entry point for the application and acts as the main controller between the
 * logic part of the simulation (see {#code Simulation} in ), and the interface part that displays the simulation
 * (see class {@code Interface}).
 * </p>
 * <p>
 * The main purpose of this class is to pass the simulation parameters set by the user in the class {@code Interface}
 * to create the simulation, and then to relay back the current state of the simulation to the {@code Interface} at
 * certain time steps so that it can be drawn on the screen.
 * </p>
 * <p>
 * This class controls the three buttons in the interface:
 * </p>
 * <ol>
 * <li> Create
 * <li> Start
 * <li> Reset
 * </ol>
 *
 */
public class Evo {

    private static Interface evoPanel;
    private static SimulationWorker evoWorker;
    private boolean cancelled = true;

    private String DEFAULT_DIRECTORY = "images";

    /**
     * <p>
     * HashMap containing the list of parameters and their values.
     * </p>
     * <p>
     *     For each parameter, its name and its value are represented as a pair of Strings
     *     in the HashMap
     * </p>
     */
    private HashMap<String,String> parameterValues;

    private int noOfSteps;
    private int noOfTrials;

    private int gridHeight;
    private int gridWidth;

    private Random rgen;    /*nDEBUG*/

    private int seed;

    private double bestFit;
    private int bestSeed;

    private int[][] targetInfo;

    private Simulation parent;

    public Evo(String mode){
        if (mode.equals("graphical")) {
            evoPanel = new Interface();
            createAndShowGUI();
        }
        else if(mode.equals("cli"))
        {

        }
    }


    /**
     * Add listeners to the buttons:
     *
     * The create button calls
     */
    private void createAndShowGUI(){

        final JFileChooser fc;
        fc = new JFileChooser(DEFAULT_DIRECTORY);
        evoPanel.loadButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Open dialog " + e);
                        FileNameExtensionFilter filter = new FileNameExtensionFilter("PGM Images", "pgm");
                        fc.setFileFilter(filter);
                        int returnVal = fc.showOpenDialog(evoPanel);
                        if (returnVal == JFileChooser.APPROVE_OPTION){
                            loadImage(fc.getSelectedFile());
                            //System.out.println("You chose " + fc.getSelectedFile().getName());
                            if (evoPanel.getAnimationPanel().indexOfTab("Target") != -1)
                                evoPanel.getAnimationPanel().removeAll();
                            evoPanel.createAnimationGrids(gridHeight, gridWidth);
                            evoPanel.updateTargetGrid(targetInfo);

                            evoPanel.getAnimationPanel().setSelectedIndex(2);
                            evoPanel.createButton.setEnabled(true);
                        }
                    }
                });

        evoPanel.createButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Pressed create " + e);

                        parameterValues = evoPanel.getParameterValues();
                        //evoPanel.updateGrid(simulation.getAgentState(),simulation.getPeptideState(),targetInfo);
                        bestFit = gridHeight * gridWidth;
                        seed = InterfaceHelper.extract_integer(parameterValues.get("Random Seed"));
                        parent = createSimulation(seed);
                        evoPanel.updateGrid(parent.getAgentState(), parent.getPeptideState(), targetInfo);
                        evoPanel.updateInfoPanel(parent.getValues());
                        evoPanel.getAnimationPanel().setSelectedIndex(0);
                        evoPanel.startButton.setEnabled(true);
                    }
                }
        );

        evoPanel.startButton.addActionListener(
                new ActionListener(){
                    public void actionPerformed(ActionEvent e){
                        cancelled = false;
                        System.out.println("Pressed start " + e);
                        evoWorker = startSimulation();
                        System.out.println(evoWorker);

                        evoPanel.stopButton.setEnabled(true);
                        evoPanel.startButton.setEnabled(false);
                    }
                }
        );

        evoPanel.stopButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Pressed stop " + e);
                        System.out.println(evoWorker);
                        cancelled = true;
                        //evoWorker.cancel(true);
                    }
                }
        );

        evoPanel.startButton.setEnabled(false);
        evoPanel.createButton.setEnabled(false);
        evoPanel.stopButton.setEnabled(false);
    }

    /**
     * Initialise a simulation with the parameters from the interface.
     *
     * @return Simulation object
     */
    private Simulation createSimulation(){
        seed = InterfaceHelper.extract_integer(parameterValues.get("Random Seed"));
        return createSimulation(seed);
    }

    /**
     * Initialise a simulation with a fixed random seed (the other parameters from the interface).
     *
     * @return Simulation object
     */
    private Simulation createSimulation(int inputSeed){
        rgen = new Random(inputSeed);
        noOfSteps = InterfaceHelper.extract_integer(parameterValues.get("Steps per Trial"));
        noOfTrials = InterfaceHelper.extract_integer(parameterValues.get("Number of Trials"));

        return new Simulation(
                gridHeight,
                gridWidth,
                rgen,
                targetInfo,

                InterfaceHelper.extract_integer(parameterValues.get("Number of Agents")),
                InterfaceHelper.extract_integer(parameterValues.get("Max Colours")),
                InterfaceHelper.extract_integer(parameterValues.get("Max Motility")),
                InterfaceHelper.extract_integer(parameterValues.get("Max Sensitivity")),
                InterfaceHelper.extract_integer(parameterValues.get("Max Agent Lifespan")),
                InterfaceHelper.extract_double(parameterValues.get("Max Survival Rate")),
                InterfaceHelper.extract_double(parameterValues.get("Max Reproduction Rate")),

                InterfaceHelper.extract_integer(parameterValues.get("Max Peptides Produced")),
                InterfaceHelper.extract_integer(parameterValues.get("Starting Peptides")),
                InterfaceHelper.extract_integer(parameterValues.get("Max Diffusion Rate")),
                InterfaceHelper.extract_double(parameterValues.get("Max Expiry Rate"))
        );
    }


    public String createAndRunSimulation(String filename,
                                         int simulationNoOfTrials, int simulationStepsPerTrial, int inputSeed,
                                         int noOfAgents, int maxMotility, int maxSensitivity, int maxColours,
                                         int maxAgentLifespan, double maxSurvivalRate, double maxReproductionRate,
                                         int maxPeptidesProduced, int startingPeptides, int maxDiffusionRate, double peptideExpiry){
        File inputFile = new File(filename);
        loadImage(inputFile);


        rgen = new Random(inputSeed);
        // need to rename var, since parent is already used
        Simulation localParent = new Simulation(
                gridHeight,
                gridWidth,
                rgen,
                targetInfo,
                noOfAgents,
                maxColours,
                maxMotility,
                maxSensitivity,
                maxAgentLifespan,
                maxSurvivalRate,
                maxReproductionRate,
                maxPeptidesProduced,
                startingPeptides,
                maxDiffusionRate,
                peptideExpiry);

        //printAgentGrid(localParent.getAgentState());
        for (int i = 0; i < simulationNoOfTrials; i++){
            //System.out.println("k is : " + i);
            Simulation child = new Simulation(localParent);
            for (int j = 0; j < simulationStepsPerTrial; j++) {
                child.update();
            }
            if (child.calcFitness() <= localParent.calcFitness()) {
                localParent = new Simulation(child);
            }
            child.mutate();
        }

        String result = "";
        result = result + simulationNoOfTrials + ",";
        result = result + simulationStepsPerTrial + ",";
        result = result + inputSeed + ",";
        result = result + noOfAgents + ",";
        result = result + maxMotility + ",";
        result = result + maxSensitivity + ",";
        result = result + maxColours + ",";
        result = result + maxAgentLifespan + ",";
        result = result + maxSurvivalRate + ",";
        result = result + maxReproductionRate + ",";
        result = result + maxPeptidesProduced + ",";
        result = result + startingPeptides + ",";
        result = result + maxDiffusionRate + ",";
        result = result + peptideExpiry + ",";
        result = result + localParent.calcFitness();

        return result;
    }

    /**
     * Starts the simulation.
     */
    private SimulationWorker startSimulation(){
        SimulationWorker worker = new SimulationWorker();
        worker.execute();
        return worker;
    }


    private class SimulationWorker extends SwingWorker<Integer,String>{

        public SimulationWorker() {
        }

        @Override
        protected Integer doInBackground() throws Exception {

            HashMap<String,String> infoValues;
            int parentChange = 0;

            for (int k = 0; k < noOfTrials && !cancelled; k++){
                System.out.println("k is : " + k);
                Simulation child = new Simulation(parent);
                infoValues = child.getValues();
                infoValues.put("Turn",Integer.toString(k));
                infoValues.put("Parent Change",Integer.toString(parentChange));
                evoPanel.updateInfoPanel(infoValues);
                evoPanel.updateGrid(child.getAgentState(), child.getPeptideState(), targetInfo);
                for (int i = 0; i < noOfSteps && !cancelled; i++){
                    child.update();
                }
                if (child.calcFitness() <= parent.calcFitness()){
                    evoPanel.updateGrid(child.getAgentState(), child.getPeptideState(), targetInfo);
                    parent = new Simulation(child);
                    parentChange++;
                    System.out.println("Parent change");
                }
                child.mutate();
            }
            evoPanel.updateGrid(parent.getAgentState(), parent.getPeptideState(), targetInfo);
            return 1;
        }

        @Override
        protected void process(List<String> chunks) {
        }
    }

    private void loadImage(File inputFile) {
        int  maxvalue;

        FileInputStream fstream;
        Scanner reader;
        try {
            fstream = new FileInputStream(inputFile);
            reader = new Scanner(fstream);
            reader.nextLine();
            reader.nextLine();
            gridWidth = reader.nextInt();
            gridHeight = reader.nextInt();
            maxvalue = reader.nextInt();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        targetInfo = new int[gridHeight][gridWidth];
        for (int row = 0; row < gridHeight; row++) {
            for (int col = 0; col < gridWidth; col++) {
                targetInfo[row][col] = reader.nextInt();
            }
        }
        try{
            fstream.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }


    }

    private void printAgentGrid(int[][] agentGrid){
        for(int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                System.out.print(agentGrid[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

}



