package main.imagecreator;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.Scanner;

public class Flagmaker {

    public static int height;
    public static int width;
    public static String DIRNAME = "images/";

    public static void main(String[] args)
    {
        height = 80;
        width = 120;
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(DIRNAME + "frenchflag_" + width + "_" + height + ".pgm"));
            out.write("P2\n");
            out.write("# French Flag\n");
            out.write(3*(width/3) + " " + height + "\n");
            out.write("3\n");
            for(int i = 0; i < height; i++){
                for (int j = 0; j < width/3; j++){
                    out.write("1 ");
                }
                for (int j = 0; j < width/3; j++){
                    out.write("2 ");
                }
                for (int j = 0; j < width/3; j++){
                    out.write("3 ");
                }
                out.write('\n');
            }
            out.close();
        } catch (Exception e){}
    }
}
