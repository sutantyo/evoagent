package main.imagecreator;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class BullseyeMaker {

    public static int height;
    public static int width;
    public static String DIRNAME = "images/";

    // Source: https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

    public static void main(String[] args)
    {
        // The heights and width must be an odd number (for now)
        height = 35;
        width = 35;
        try{
            int[][] output = new int[height][width];
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    output[i][j] = 0;

            BufferedWriter out = new BufferedWriter(new FileWriter(DIRNAME + "bullseye_" + width + "_" + height + ".pgm"));
            out.write("P2\n");
            out.write("# Bull's Eye \n");
            out.write(width + " " + height + "\n");
            out.write("3\n");


            int color = 1;
            for (int radius = height/2; radius > 0; radius--) {
                if (radius < 2 * (height/2/3))
                    color = 2;
                if (radius < (height/2/3))
                    color = 3;

                int x = radius;
                int y = 0;
                int x0 = height / 2;
                int y0 = width / 2;
                int decisionOver2 = 1 - x;

                while (y <= x) {
                    output[x + x0][y + y0] = color;
                    output[y + x0][x + y0] = color;
                    output[-x + x0][y + y0] = color;
                    output[-y + x0][x + y0] = color;

                    output[-x + x0][-y + y0] = color;
                    output[-y + x0][-x + y0] = color;
                    output[x + x0][-y + y0] = color;
                    output[y + x0][-x + y0] = color;
                    y++;
                    if (decisionOver2 <= 0) {
                        decisionOver2 = decisionOver2 + 2 * y + 1;
                    } else {
                        x--;
                        decisionOver2 = decisionOver2 + 2 * (y - x) + 1;
                    }
                }
            }
            for (int i = 0; i < height; i++){
                for (int j = 0; j < width-2; j++) {
                   if (output[i][j] > 0 && output[i][j+2] > 0 && output[i][j+1] == 0 )
                       output[i][j+1] = (output[i][j] > output[i][j+2] ? output[i][j] : output[i][j+2]);
                }
            }

            for (int i = 0; i < height; i++){
                for (int j = 0; j < width; j++){
                    out.write(String.valueOf(output[i][j])+" ");
                }
                out.write('\n');
            }

            out.close();
        } catch (Exception e){}
    }
}
