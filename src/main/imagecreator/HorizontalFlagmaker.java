package main.imagecreator;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class HorizontalFlagmaker {

    public static int height;
    public static int width;
    public static String DIRNAME = "images/";

    public static void main(String[] args)
    {
        height = 80;
        width = 120;
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(DIRNAME + "frenchflag_h_" + width + "_" + height + ".pgm"));
            out.write("P2\n");
            out.write("# French Flag\n");
            out.write(width + " " + 3*(height/3) + "\n");
            out.write("3\n");
            for(int i = 0; i < height/3; i++) {
                for (int j = 0; j < width; j++) {
                    out.write("1 ");
                }
            }
            for(int i = 0; i < height/3; i++) {
                for (int j = 0; j < width; j++) {
                    out.write("2 ");
                }
            }
            for(int i = 0; i < height/3; i++) {
                for (int j = 0; j < width; j++){
                    out.write("3 ");
                }
                out.write('\n');
            }
            out.close();
        } catch (Exception e){}
    }
}
