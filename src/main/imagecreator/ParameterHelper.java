package main.imagecreator;

import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;

/**
 * The following class is a helper class designed to wrap together the input parameters of the simulation
 * so that they can be passed around more easily.
 * Before this class was written, in order to create a Simulation, we needed to pass over 10 input parameters
 * to the constructor.
 * The number and types of parameters frequently changed during development, and since the use of these
 * parameters are repeated in several parts of the code, this resulted in a fragile code which must
 * be modified carefully on every change.
 *
 * In this class, each parameter is a class variable with its own getter and setter.
 * Parameters can thus be added/modified more safely, and there is less risk of using the
 * wrong value for a parameter.
 * If a parameter is deleted, then the loss of its getter/setter will cause a compilation error
 * if any part of the code is still trying to use the parameter.
 *
 * Most importantly, it gives a much cleaner code whenever there is a function call involving these parameters,
 * and also easier to debug when a mistake is made.
 *
 * @see main.agent.Simulation
 */
public class ParameterHelper {

    private int seed;
    private int numberOfSimulation;
    private int numberOfStepsPerSimulation;

    private int gridHeight;
    private int gridWidth;
    private int[][] targetInfo;

    private int numberOfAgents;
    private int maxMotility;
    private int maxSensitivity;
    private int maxAgentLifespan;
    private double maxSurvivalRate;
    private double maxReproductionRate;

    private int maxPeptidesProduced;
    private int startingSignalPeptides;
    private int maxDiffusionRate;
    private int maxPeptideExpiry;

    private void loadImage(File inputFile) {
        int  maxvalue;

        FileInputStream fstream;
        Scanner reader;
        try {
            fstream = new FileInputStream(inputFile);
            reader = new Scanner(fstream);
            reader.nextLine();
            reader.nextLine();
            gridWidth = reader.nextInt();
            gridHeight = reader.nextInt();
            maxvalue = reader.nextInt();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }
        targetInfo = new int[gridHeight][gridWidth];
        for (int row = 0; row < gridHeight; row++) {
            for (int col = 0; col < gridWidth; col++) {
                targetInfo[row][col] = reader.nextInt();
            }
        }
        try{
            fstream.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return;
        }


    }


    // The rest of the file should only have getters and setters

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public int getNumberOfSimulation() {
        return numberOfSimulation;
    }

    public void setNumberOfSimulation(int numberOfSimulation) {
        this.numberOfSimulation = numberOfSimulation;
    }

    public int getNumberOfStepsPerSimulation() {
        return numberOfStepsPerSimulation;
    }

    public void setNumberOfStepsPerSimulation(int numberOfStepsPerSimulation) {
        this.numberOfStepsPerSimulation = numberOfStepsPerSimulation;
    }

    public int getGridHeight() {
        return gridHeight;
    }

    public void setGridHeight(int gridHeight) {
        this.gridHeight = gridHeight;
    }

    public int getGridWidth() {
        return gridWidth;
    }

    public void setGridWidth(int gridWidth) {
        this.gridWidth = gridWidth;
    }

    public int[][] getTargetInfo() {
        return targetInfo;
    }

    public void setTargetInfo(int[][] targetInfo) {
        this.targetInfo = targetInfo;
    }

    public int getNumberOfAgents() {
        return numberOfAgents;
    }

    public void setNumberOfAgents(int numberOfAgents) {
        this.numberOfAgents = numberOfAgents;
    }

    public int getMaxMotility() {
        return maxMotility;
    }

    public void setMaxMotility(int maxMotility) {
        this.maxMotility = maxMotility;
    }

    public int getMaxSensitivity() {
        return maxSensitivity;
    }

    public void setMaxSensitivity(int maxSensitivity) {
        this.maxSensitivity = maxSensitivity;
    }

    public int getMaxAgentLifespan() {
        return maxAgentLifespan;
    }

    public void setMaxAgentLifespan(int maxAgentLifespan) {
        this.maxAgentLifespan = maxAgentLifespan;
    }

    public double getMaxSurvivalRate() {
        return maxSurvivalRate;
    }

    public void setMaxSurvivalRate(double maxSurvivalRate) {
        this.maxSurvivalRate = maxSurvivalRate;
    }

    public double getMaxReproductionRate() {
        return maxReproductionRate;
    }

    public void setMaxReproductionRate(double maxReproductionRate) {
        this.maxReproductionRate = maxReproductionRate;
    }

    public int getMaxPeptidesProduced() {
        return maxPeptidesProduced;
    }

    public void setMaxPeptidesProduced(int maxPeptidesProduced) {
        this.maxPeptidesProduced = maxPeptidesProduced;
    }

    public int getStartingSignalPeptides() {
        return startingSignalPeptides;
    }

    public void setStartingSignalPeptides(int startingSignalPeptides) {
        this.startingSignalPeptides = startingSignalPeptides;
    }

    public int getMaxDiffusionRate() {
        return maxDiffusionRate;
    }

    public void setMaxDiffusionRate(int maxDiffusionRate) {
        this.maxDiffusionRate = maxDiffusionRate;
    }

    public int getMaxPeptideExpiry() {
        return maxPeptideExpiry;
    }

    public void setMaxPeptideExpiry(int maxPeptideExpiry) {
        this.maxPeptideExpiry = maxPeptideExpiry;
    }

    // If you want to add changes to this file, please add them before the list of getters and setters.

}
