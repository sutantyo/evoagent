package main.agent;
/*-------------------------------------------------------------------------------------------------------------------*\
|                                                    Class Agent                                                      |
|                       Individual agents which interact via defined input/output channels                            |
|                                           Author: Jennifer Hallinan                                                 |
|                                             Commenced: 19/10/2014                                                   |
|                                            Last edited: 28/04/2015                                                  |
\*-------------------------------------------------------------------------------------------------------------------*/

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Agent {


    /**
     * Constructor
     *
     * @param sensitivity
     * @param peptideProduced
     * @param motility
     * @param colour
     * @param lifeSpan
     * @param survivalRate
     * @param chanceToReproduce
     * @param xLoc
     * @param yLoc
     */
    public Agent(int sensitivity, int peptideProduced, int motility, int colour, int lifeSpan, double survivalRate, double chanceToReproduce, int xLoc, int yLoc)
    {
        this.sensitivity = sensitivity;
        this.motility = motility;
        this.numOSP = peptideProduced;
        this.colour = colour;
        this.xLoc = xLoc;
        this.yLoc = yLoc;

        this.lifeSpan = lifeSpan;
        this.survivalRate = survivalRate;
        this.chanceToReproduce = chanceToReproduce;

        this.age = 0;
        this.survivalChance = 1;
    }

    /**
     * Random constructor
     *
     * @param rgen  the random number generator
     * @param maxSensitivity
     * @param maxMotility
     * @param maxColour
     * @param xLoc
     * @param yLoc
     */
    public Agent(Random rgen, int maxSensitivity, int maxPeptideProduced, int maxMotility, int maxColour, int lifeSpan, double survivalRate, double chanceToReproduce, int xLoc, int yLoc)
    {
        this(   1 + rgen.nextInt(maxSensitivity),
                1 + rgen.nextInt(maxPeptideProduced),
                1 + rgen.nextInt(maxMotility),
                1 + rgen.nextInt(maxColour),
                1 + rgen.nextInt(lifeSpan),
                survivalRate,
                chanceToReproduce,
                xLoc,
                yLoc);
    }

    /**
     * Copy constructor for Agent
     *
     * @param source
     */
    public Agent(Agent source)
    {
        this(source.sensitivity, source.numOSP, source.motility, source.colour, source.lifeSpan, source.survivalRate, source.chanceToReproduce, source.xLoc, source.yLoc);
    }

    public String toString(){
        return "(" + this.getXLoc() + "," + this.getYLoc() + ")" + " c: " + this.getColour();
    }
    /**
     * Age the agent by inc years and then return its new age
     * @param inc
     * @return
     */
    public int age(int inc){
        this.age = this.age + inc;
        for (int i = 0; i < inc; i++)
            this.survivalChance = this.survivalChance * this.survivalRate;
        return this.age;
    }

    public int getLifespan(){
       return this.lifeSpan;
    }

    /**
     * Move the agent to another square.
     *
     * @param rgen the random number generator
     * @param grid the grid
     */
    public void move(Random rgen, Grid grid) {

        //System.out.println("... ... calling random to move agent");
        int distanceMoved = 0 + rgen.nextInt(this.motility);
        int startingX = this.xLoc;
        int startingY = this.yLoc;
        int newX = startingX;
        int newY = startingY;
        // Randomly move an agent, 0: N, 1: NE, ... 7: NW
        int direction = rgen.nextInt(8);
                                                                                                                            //System.out.println("Moving (" + xLoc + "," + yLoc + ") with m:" + distanceMoved + " to " + direction);
                                                                                                                            //System.out.println("...removing agent from ("+startingX+","+startingY+")");
        grid.at(startingX,startingY).removeAgent(this);
        switch (direction) {
            case 0: // north
                for(int i = 0; i < distanceMoved; i++){
                    if (newX - 1 < 0 || grid.at(newX-1,yLoc).isOccupied())
                        break;
                    newX = newX-1;
                }
                break;
            case 1: // north-east
                for(int i = 0; i < distanceMoved; i++){
                    if (newX - 1 < 0 || newY + 1 >= grid.getGridWidth() || grid.at(newX-1,newY+1).isOccupied())
                        break;
                    newX = newX-1;
                    newY = newY+1;
                }
                break;
            case 2: // east
                for(int i = 0; i < distanceMoved; i++){
                    if (newY + 1 >= grid.getGridWidth() || grid.at(xLoc,newY+1).isOccupied())
                        break;
                    newY = newY+1;
                }
                break;
            case 3: // south-east
                for(int i = 0; i < distanceMoved; i++){
                    if (newX + 1 >= grid.getGridHeight() || newY + 1 >= grid.getGridWidth() || grid.at(newX+1,newY+1).isOccupied())
                        break;
                    newX = newX+1;
                    newY = newY+1;
                }
                break;
            case 4: // south
                for(int i = 0; i < distanceMoved; i++){
                    if (newX + 1 >= grid.getGridHeight() || grid.at(newX+1,yLoc).isOccupied())
                        break;
                    newX = newX+1;
                }
                break;
            case 5: // south-west
                for(int i = 0; i < distanceMoved; i++) {
                    if (newX + 1 >= grid.getGridHeight() || newY - 1 < 0 || grid.at(newX + 1, newY - 1).isOccupied())
                        break;
                    newX = newX + 1;
                    newY = newY - 1;
                }
                break;
            case 6: // west
                for(int i = 0; i < distanceMoved; i++){
                    if (newY - 1 < 0 || grid.at(xLoc,newY-1).isOccupied())
                        break;
                    newY = newY-1;
                }
                break;
            case 7: // north-west
                for(int i = 0; i < distanceMoved; i++) {
                    if (!grid.isValidLocation(newX-1,newY-1) || grid.at(newX - 1, newY - 1).isOccupied())
                        break;
                    newX = newX - 1;
                    newY = newY - 1;
                }
                break;
            default:
        }
        this.xLoc = newX;
        this.yLoc = newY;
                                                                                                                            //System.out.println("...moved to (" + this.xLoc + "," + this.yLoc + ")");
        grid.at(newX,newY).addAgent(this);
    }

    /**
     * Create an exact copy of the current agent in an adjacent square
     *
     * @param rgen
     * @param grid
     * @return
     */
    public Agent reproduce(Random rgen, Grid grid){
        ArrayList<GridSquare> surroundingSquares = new ArrayList<GridSquare>();
        Agent toAdd = null;
        for (int i = -1; i <= 1; i++){
            for (int j = -1; j <= 1; j++){
                if(grid.isValidLocation(this.xLoc+i,this.yLoc+j) && grid.at(this.xLoc+i,this.yLoc+j).isEmpty()){
                   surroundingSquares.add(grid.at(this.xLoc+i,this.yLoc+j));
                }
            }
        }
        if (!surroundingSquares.isEmpty()) {
                                                                                                                            //System.out.print("...will reproduce");
            Collections.shuffle(surroundingSquares, rgen);
            GridSquare targetGrid = surroundingSquares.get(0);
                                                                                                                            //System.out.println(" in (" + targetGrid.getxLoc() + "," + targetGrid.getyLoc() + ")");
            toAdd = new Agent(this.getSensitivity(),this.getNumSP(),this.getMotility(),this.getColour(),this.getLifespan(),this.getSurvivalRate(),this.getChanceToReproduce(),targetGrid.getxLoc(),targetGrid.getyLoc());
        }
        return toAdd;
    }

    /**
     * Check if an agent should produce any peptides.
     *
     * Check the agent's surrounding grids to determine whether or not the agent should
     * produce signal peptides. The number of signal peptides in the squares above, below,
     * to the left, and to the right of the agent's current location are tallied, and
     * then checked to see if it exceeds the agent's sensitivity (in which case the
     * agent will produce peptides).
     *
     * @param g the grid
     * @return  true if the agent should produce peptides, false otherwise.
     */
    public Boolean producePeptides(Grid g) {
        int threshold = 0;
        // check left
        int nextY = this.getYLoc() - 1;
        if (nextY >= 0) {
            threshold += g.getSignalPeptideConc(this.getXLoc(), nextY);
        }
        // check right
        nextY = this.getYLoc() + 1;
        if (nextY < g.getGridWidth()) {
            threshold += g.getSignalPeptideConc(this.getXLoc(), nextY);
        }
        // check up
        int nextX = this.getXLoc() - 1;
        if (nextX >= 0){
            threshold += g.getSignalPeptideConc(nextX, this.getYLoc());
        }
        // check down
        nextX = this.getXLoc() + 1;
        if (nextX < g.getGridHeight()) {
                threshold += g.getSignalPeptideConc(nextX, this.getYLoc());
        }
        return (threshold >= this.sensitivity);
    }

    /**
     * Randomly mutate an agent's parameter
     *
     * @param rgen
     * @param sensitivity
     * @param motility
     * @param numOSP
     */
    public void mutate(Random rgen, int sensitivity, int motility, int numOSP) {
        int which = rgen.nextInt(4);            // five parameters which can be mutated
        switch (which) {
            case 0:
                int delta = rgen.nextInt((sensitivity * 2) - sensitivity); // want negative and positive values
                if (delta > 0) {
                    this.sensitivity += delta;
                }
                break;
            case 1:
                delta = rgen.nextInt(motility * 2) - motility;
                if (delta > 0) {
                    this.motility += delta;
                }
                break;
            case 2:
                this.numOSP = rgen.nextInt(numOSP);
                break;
            case 3:
                delta = rgen.nextInt(this.lifeSpan * 2) - this.lifeSpan;
                if (delta > 0) {
                    this.lifeSpan += delta;
                }
        }
    }
    // private variables
    private int xLoc;                                       // row of grid
    private int yLoc;                                       // column of grid
    private int sensitivity;                                // min sensitivity of promoter to input molecule number
    private int motility;                                   // how many cells does it move in a timestep?
    private int colour;
    private int numOSP;                                     // number of signal peptides to produce
    private double chanceToReproduce;

    private int age;
    private int lifeSpan;
    private double survivalRate;
    private double survivalChance;

    public void setColour(int c){this.colour = c;};
    public void setXLoc(int inX) {this.xLoc = inX;}
    public void setYLoc(int inY) {this.yLoc = inY;}
    public void setSensitivity(int inSens) {this.sensitivity = inSens;}
    public void setMotility(int inMot) {this.motility = inMot;}
    public void setNumOSP(int inNum) {this.numOSP = inNum;}
    public int getColour() {return this.colour;}
    public int getNumSP() {return this.numOSP;}
    public int getXLoc() {return this.xLoc;}
    public int getYLoc() {return this.yLoc;}
    public int getSensitivity() {return this.sensitivity;}
    public int getMotility() {return this.motility;}
    public double getChanceToReproduce() {return this.chanceToReproduce;}
    public double getSurvivalRate() {return this.survivalRate;}
    public double getSurvivalChance() {return this.survivalChance;}
}
