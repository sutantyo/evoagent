package main.agent;
/*-------------------------------------------------------------------------------------------------------------------*\
|                                                       Class AnimationGrid                                                    |
|                                       The landscape upon which the agents live                                      |
|                                          Square, currently no wraparound                                            |
|                                             Author: Jennifer Hallinan                                               |
|                                               Commenced: 19/10/2014                                                 |
|                                              Last edited: 28/04/2014                                                |
\*-------------------------------------------------------------------------------------------------------------------*/

public class Grid
{
    // private variables
    private GridSquare[][] theGrid;
    private int gridHeight;
    private int gridWidth;

    // Constructor
    public Grid (int gridHeight, int gridWidth)
    {
        this.theGrid = new GridSquare[gridHeight][gridWidth];
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;

        for (int i = 0; i < gridHeight; i++)
        {
            for (int j = 0; j < gridWidth; j++)
            {
                this.theGrid[i][j] = new GridSquare(i,j);
            }
        }
    }

    public Grid (Grid source){
        this.gridHeight = source.gridHeight;
        this.gridWidth = source.gridWidth;
        this.theGrid = new GridSquare[this.gridHeight][this.gridWidth];
        for(int i = 0; i < this.gridHeight; i++){
            for(int j = 0; j < this.gridWidth; j++) {
                this.theGrid[i][j] = new GridSquare(source.at(i,j));
            }
        }
    }

    /**
     * Add an agent to the grid.
      * @param a
     */
    public void addAgent(Agent a){
        this.theGrid[a.getXLoc()][a.getYLoc()].addAgent(a);
    }

    /**
     * Add a peptide to the grid
     * @param p
     */
    public void addPeptide(Peptide p){
        this.theGrid[p.getXLoc()][p.getYLoc()].addPeptide(p);
    }

    /**
     * Return the GridSquare at coordinate (i,j)
     *
     * @param i
     * @param j
     * @return
     */
    public GridSquare at(int i, int j){
        return theGrid[i][j];
    }

    /**
     * Return the number of peptides in GridSquare(row,col)
     */
    public int getSignalPeptideConc(int row, int col)
    {
        GridSquare gc = this.theGrid[row][col];
        return(gc.getSignalPeptideConc());
    }

    /**
     * Increase concentration of peptide in GridSquare(row,col) by n
     * @param row
     * @param col
     * @param n
     */
    public void updateGCOSPDensity(int row, int col, int n)
    {
        this.theGrid[row][col].incSignalPeptideConc(n);
    }

    /**
     * Return the height of the Grid
     * @return height of the Grid
     */
    public int getGridHeight() {return this.gridHeight;}

    /**
     * Return the width of the Grid
     * @return width of the Grid
     */
    public int getGridWidth() {return this.gridWidth;}


    public Boolean isValidLocation(int xLoc, int yLoc){
        if (xLoc >= 0 && xLoc < getGridHeight() && yLoc >= 0 && yLoc < getGridWidth())
            return true;
        return false;
    }

}
