package main.agent;
/*-------------------------------------------------------------------------------------------------------------------*\
|                                              Class GridSquare                                                         |
|                           An element of a grid; keeps track of its contents                                         |
|                                      Author: Jennifer Hallinan                                                      |
|                                       Commenced: 19/10/2014                                                         |
|                                      Last edited: 23/03/2014                                                        |
\*-------------------------------------------------------------------------------------------------------------------*/

import java.util.ArrayList;

public class GridSquare
{
    // private variables
    private int signalPeptideConc;              // number of molecules of each type of signal peptide

    private ArrayList<Agent> agents;
    private ArrayList<Peptide> peptides;
    private int xLoc;
    private int yLoc;

    // constructor
    public GridSquare(int xLoc, int yLoc){
        this.agents = new ArrayList<Agent>();
        this.peptides = new ArrayList<Peptide>();
        this.signalPeptideConc = 0;
        this.xLoc = xLoc;
        this.yLoc = yLoc;
    }

    // copy constructor
    public GridSquare ( GridSquare source ){
        this.agents = new ArrayList<Agent>();
        for (int i = 0; i < source.agents.size(); i++)
            this.agents.add(new Agent(source.agents.get(i)));
        this.peptides = new ArrayList<Peptide>();
        for (int i = 0; i < source.peptides.size(); i++)
            this.peptides.add(new Peptide(source.peptides.get(i)));
        this.signalPeptideConc = source.signalPeptideConc;
        this.xLoc = source.xLoc;
        this.yLoc = source.yLoc;
    }

    /**
     * Checks if a GridSquare is empty (contains no agent)
     *
     * @return true if GridSquare is empty
     */
    public boolean isEmpty(){
        return agents.isEmpty();
    }
    /**
     * Checks if a GridSquare is occupied by an agent
     *
     * @return true if GridSquare is occupied
     */
    public boolean isOccupied(){
        return !agents.isEmpty();
    }

    /**
     * Add agent a to this grid square
     * @param a
     */
    public void addAgent(Agent a) {
        try {
            if (this.isOccupied())
                throw new SquareOccupiedException("Attempting to add agent to an occupied square (" + this.getxLoc() + "," + this.getyLoc() + ")");
            else {
                                                                                                                            //System.out.println("Added agent at (" + a.getXLoc() + "," + a.getYLoc() + ")");
                agents.add(a);
            }
        }
        catch(SquareOccupiedException e){
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

    }

    /**
     * Remove agent a from this grid square
     * @param a
     */
    public void removeAgent(Agent a)
    {
        agents.remove(0);
    }

    /**
     * Add peptide p to this grid square
     * @param p
     */
    public void addPeptide(Peptide p) {
        peptides.add(p);
        incSignalPeptideConc(1);
    }

    public void removePeptide(Peptide p){
        peptides.remove(p);
        decreaseSignalPeptideConcentration(1);
    }

    public void incSignalPeptideConc(int howmany)
    {
        this.signalPeptideConc += howmany;
    }

    public void decreaseSignalPeptideConcentration(int n)
    {
        this.signalPeptideConc = this.signalPeptideConc - n;
    }


    public int getSignalPeptideConc()
    {
        return (this.signalPeptideConc);
    }

    public int getFitValue(){
        if (this.isOccupied())
            return agents.get(0).getColour();
        else
            return 0;
    }

    public int getxLoc(){
        return xLoc;
    }

    public int getyLoc(){
        return yLoc;
    }

    class SquareOccupiedException extends Exception{
        public SquareOccupiedException(String message) {
            super(message);
        }
    }
}
