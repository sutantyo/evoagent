package main.agent;

import java.util.Random;

/*-------------------------------------------------------------------------------------------------------------------*\
|                                               Class Peptide                                                      |
|                 Defines the characteristics of a signal peptide for communicating between cells                     |
|                                         Author: Jennifer Hallinan                                                   |
|                                          Commenced: 19/10/2014                                                      |
|                                         Last edited: 05/04/2015                                                     |
\*-------------------------------------------------------------------------------------------------------------------*/

public class Peptide
{
    /**
     * Create a Peptide at a location (x,y)
     *
     * @param rgen
     * @param diffusionRate
     * @param type
     */
    public Peptide(Random rgen, int diffusionRate, double expiryRate, String type, int xLoc, int yLoc)
    {
        this.type = type;
        this.diffusionRate = diffusionRate;
        this.expiryRate = expiryRate;
        this.xLoc = xLoc;
        this.yLoc = yLoc;
    }

    public Peptide(Peptide source){
        this.type = source.type;
        this.diffusionRate = source.diffusionRate;
        this.expiryRate = source.expiryRate;
        this.xLoc = source.xLoc;
        this.yLoc = source.yLoc;
    }

    /**
     * Diffuse (move) the peptides at random
     *
     * @param rgen the random number generator
     * @param grid the grid
     */
    public void diffuse(Random rgen, Grid grid)
    {
        // decide direction - only four direct neighbours
        int direction = rgen.nextInt(4);
        int newX = 0;
        int newY = 0;
        grid.at(this.getXLoc(),this.getYLoc()).removePeptide(this);
        switch(direction)
        {
            case 0:                     // left
                newY = this.yLoc - this.diffusionRate;
                if (newY < 0)
                {
                    newY = 0;
                }
                // System.out.println("Moving left, newY is " + newY);
                this.yLoc = newY;
                break;
            case 1:                     // right
                newY = this.yLoc + this.diffusionRate;
                if (newY >= grid.getGridWidth())
                {
                    newY = grid.getGridWidth()-1;
                }
                // System.out.println("Moving right, newY is " + newY);
                this.yLoc = newY;
                break;
            case 2:                     // up
                newX = this.xLoc - this.diffusionRate;
                if (newX < 0)
                {
                    newX = 0;
                }
                // System.out.println("Moving ip, newX is " + newX);
                this.xLoc = newX;
                break;
            case 3:                     // down
                newX = this.xLoc + this.diffusionRate;
                if (newX >= grid.getGridHeight())
                {
                    newX = grid.getGridHeight()-1;
                }
                // System.out.println("Moving down, newX is " + newX);
                this.xLoc = newX;
        }
        grid.at(this.getXLoc(),this.getYLoc()).addPeptide(this);
        // System.out.println();
    }


    // private variables
    private String type;                // signal peptide or output peptide
    private int diffusionRate;          // number of cells diffused per unit time
    private double expiryRate;      // probability of disappearing per time step (0.0 - 1.0)

    private int xLoc;                   // what row the peptide is in
    private int yLoc;                   // what column the peptide is in

    public void setXLoc(int where)
    {
        this.xLoc = where;
    }
    public void setYLoc(int where) {this.yLoc = where;}
    public String getType()
    {
        return this.type;
    }
    public int getXLoc() {
        return this.xLoc;
    }
    public int getYLoc(){
        return this.yLoc;
    }
    public int getDiffusionRate() { return (this.diffusionRate);}
    public double getExtinctionRate() { return (this.expiryRate);}

}
