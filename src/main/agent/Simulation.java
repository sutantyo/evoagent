package main.agent;/*-------------------------------------------------------------------------------------------------------------------*\
|                                                 Class main.agent.Simulation                                                      |
|                                     Main class controlling the simulation                                           |
|                                         Author: Jennifer Hallinan                                                   |
|                                          Commenced: 20/10/2014                                                      |
|                                          Last edited: 28/04/2015                                                    |
\*-------------------------------------------------------------------------------------------------------------------*/

import java.util.*;

public class Simulation {

    // Class variables from user input
    private Random rgen;


    private int gridHeight;
    private int gridWidth;

    private int numberOfAgents;                              // number of agents
    private int maxPeptidesProduced;
    private int maxAgentLifespan;
    private int maxMotility;

    private double maxReproductionRate;
    private double maxSurvivalRate;

    private int startingPeptides;                               // maximum number of output signal peptide molecules
    private int maxSensitivity;                          // maximum number of peptides needed to trigger an main.agent
    private int maxDiffusionRate;                                   // maximum diffusion rate per timestep
    private int maxColours;                              // number of output colours produced
    private double maxPeptideExpiry;

    // private variables
    private ArrayList<Agent> agents;
    private ArrayList<Peptide> peptides;
    private int startingFit;

    int[][] target;
    public Grid grid;


    HashMap<String,String> values;


    /**
     * Copies the parameters passed to the constructor and then populate the grid with agents and starting peptides.
     *
     * To populate the grid with agents, we go square by square, and in each square we generate a random number to
     * see if an agent will be created there. For example, if there are 100 squares and we want to put 20 agents
     * randomly, then for each square generate a random integer r between 0 and 100, and add an agent to that square
     * if r < 20. The number of agents will not exactly be 20, but it should be quite close.
     *
     * The starting peptides are generated differently since each square can contain more than one peptide.
     * Each peptide created is simply assigned a random x and y coordinates.
     *
     * @param gridHeight
     * @param gridWidth
     * @param rgen
     * @param numberOfAgents
     * @param maxPeptidesProduced
     * @param startingSignalPeptides
     * @param maxSensitivity
     * @param maxDiffusionRate
     */

    public Simulation(int gridHeight,
                      int gridWidth,
                      Random rgen,
                      int[][] targetInfo,

                      int numberOfAgents,
                      int maxColours,
                      int maxMotility,
                      int maxSensitivity,
                      int maxAgentLifespan,
                      double maxSurvivalRate,
                      double maxReproductionRate,
                      int maxPeptidesProduced,

                      int startingSignalPeptides,
                      int maxDiffusionRate,
                      double maxPeptideExpiry)
    {
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;
        this.rgen = rgen;
        target = targetInfo;

        this.numberOfAgents = numberOfAgents;
        this.maxMotility = maxMotility;
        this.maxSensitivity = maxSensitivity;
        this.maxAgentLifespan = maxAgentLifespan;

        this.maxSurvivalRate = maxSurvivalRate;
        this.maxReproductionRate = maxReproductionRate;
        this.maxColours = maxColours;

        this.maxPeptidesProduced = maxPeptidesProduced;
        this.startingPeptides = startingSignalPeptides;
        this.maxDiffusionRate = maxDiffusionRate;
        this.maxPeptideExpiry = maxPeptideExpiry;

        int numberOfGridCells = gridHeight*gridWidth;
        startingFit = numberOfGridCells;

        // Create a new grid, list of agents, and list of peptides
        grid = new Grid(gridHeight,gridWidth);
        agents = new ArrayList<Agent>();
        peptides = new ArrayList<Peptide>();

        // Populate the grid with agents
        for (int i = 0; i < gridHeight; i++){
            for (int j = 0; j < gridWidth; j++){
               if (rgen.nextInt(numberOfGridCells) < numberOfAgents) {
                   Agent a = new Agent(rgen, this.maxSensitivity, this.maxPeptidesProduced, this.maxMotility, this.maxColours, this.maxAgentLifespan, this.maxSurvivalRate, this.maxReproductionRate, i, j);
                   agents.add(a);
                   grid.addAgent(a);
               }
            }
        }

        // Populate the board with peptides;
        for (int i = 0; i < startingSignalPeptides; i++){
            Peptide np = new Peptide(rgen, maxDiffusionRate, maxPeptideExpiry, "signal",
                                    rgen.nextInt(gridHeight),rgen.nextInt(gridWidth));
            peptides.add(np);
            grid.addPeptide(np);
        }
        getPeptideState();

        values = new HashMap<String, String>();
        values.put("Agents",String.valueOf(agents.size()));
        values.put("Peptides",String.valueOf(peptides.size()));
        values.put("Fitness",String.valueOf(calcFitness()));
    }

    /**
     * Copy constructor
     *
     * @param source
     */
    public Simulation(final Simulation source){
        //System.out.println("Calling copy constructor");
        // Class variables from user input
        this.rgen = source.rgen;
        //System.out.println("... rgen is " + this.rgen.nextInt());

        this.gridHeight = source.gridHeight;
        this.gridWidth = source.gridWidth;


        this.numberOfAgents = source.numberOfAgents;
        this.maxPeptidesProduced = source.maxPeptidesProduced;
        this.maxAgentLifespan = source.maxAgentLifespan;
        this.maxMotility = source.maxMotility;

        this.startingPeptides = source.startingPeptides;
        this.maxSensitivity = source.maxSensitivity;
        this.maxDiffusionRate = source.maxDiffusionRate;
        this.maxColours = source.maxColours;

        this.startingFit = source.startingFit;

        this.grid = new Grid(source.gridHeight,source.gridWidth);
        this.agents = new ArrayList<Agent>();
        for (int i = 0; i < source.agents.size(); i++) {
            Agent temp = new Agent(source.agents.get(i));
            this.agents.add(temp);
            this.grid.addAgent(temp);
        }
        this.peptides = new ArrayList<Peptide>();
        for (int i = 0; i < source.peptides.size(); i++){
            Peptide temp = new Peptide(source.peptides.get(i));
            this.peptides.add(temp);
            this.grid.addPeptide(temp);
        }

        this.target = source.target.clone();

        values = new HashMap<String, String>();
        values.put("Agents",String.valueOf(agents.size()));
        values.put("Peptides",String.valueOf(peptides.size()));
        //System.out.println(calcFitness());
        values.put("Fitness",String.valueOf(calcFitness()));
    }
    /**
     * Returns the current state of the AnimationGrid
     *
     * The information is in the form of int[gridSize][gridSize]
     * where each array cell contains an integer x.
     * A value of 0 means that the cell is unoccupied, while a value x > 0 means that
     * the cell is occupied by an agent and x represents the colour of the agent in that
     * cell.
     *
      * @return an integer double array showing the colours of the agent in each cell
     */
    public int[][] getAgentState()
    {
        // Initialise the double array of integers with 0
        int[][] currentState = new int[gridHeight][gridWidth];
        for(int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                currentState[i][j] = 0;
            }
        }

        // Iterate through ArrayList<Agent> to get its location, then set the value of int[][] currentState
        // in that location to the agent's colour.
        for(Agent a : agents){
            currentState[a.getXLoc()][a.getYLoc()] = a.getColour();
        }


        /*
        for(int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                System.out.print(currentState[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        */

        return currentState;
    }

    /**
     * Returns the number of peptides in each square of the grid.
     *
     * @return an integer double array showing the number of peptides in each square
     */
    public int[][] getPeptideState()
    {

        //System.out.println("In getPeptState " + gridHeight + " " + gridWidth);
        // Initialise the double array of integers with 0
        int[][] currentState = new int[gridHeight][gridWidth];
        for(int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                currentState[i][j] = grid.getSignalPeptideConc(i,j);
                                                                                    //System.out.println("conc " + i + "," + j + " is " + grid.getSignalPeptideConc(i,j));
            }
        }

        /*
        for(int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                System.out.print(currentState[i][j] + " " );
            }
            System.out.println();
        }
        System.out.println();
        */
        return currentState;
    }

    /**
     * Run one step of the simulation:
     * 1. For each agent, move the agent and then check if it should produce peptides in the new square
     * 2. For each peptide, diffuse it
     */
    public void update() {

        // Randomise the order of ArrayList<Agent>
        Collections.shuffle(agents,rgen);

        // Iterate through the list, increasing the age of each agent and removing it if its age
        // exceeds its lifespan. Otherwise move the agent, and produce peptides (if it should)
        ListIterator<Agent> a = agents.listIterator();
        while(a.hasNext()){

            Agent currentAgent = a.next();
                                                                                                                            //System.out.println("processing agent in (" + currentAgent.getXLoc() + "," + currentAgent.getYLoc() + ")");
            // age the agent and then check if it exceeds lifespan
            //System.out.println("Checking if agent in (" + currentAgent.getXLoc() + "," + currentAgent.getYLoc() + ") will survive ");
            //System.out.println("... current surv rate is " + currentAgent.getSurvivalRate());

            if (currentAgent.age(1) < currentAgent.getLifespan() && currentAgent.getSurvivalRate() > rgen.nextDouble()) {
                                                                                                                            //System.out.println("... it survived");
                //System.out.println("... survived");
                currentAgent.move(rgen, grid);

                // Check to see if an agent will reproduce
                //System.out.println("... then check if reproduce");
                if (currentAgent.getChanceToReproduce() > rgen.nextDouble()) {
                                                                                                                            //System.out.print("... reproducing: ");
                    Agent toAdd = currentAgent.reproduce(rgen, grid);
                    if (toAdd != null) {
                                                                                                                            //System.out.println("... added agent in (" + toAdd.getXLoc() + "," + toAdd.getYLoc() + ")");
                        grid.addAgent(toAdd);
                        a.add(toAdd);
                    }
                                                                                                                            //else
                                                                                                                            //System.out.println("didn't reproduce");
                }
                // Check to see if an agent will produce peptide
                //System.out.println("... then check if produce peptide");
                if (currentAgent.producePeptides(grid)) {
                    for (int i = 0; i < currentAgent.getNumSP(); i++) {

                        Peptide np = new Peptide(rgen, maxDiffusionRate, maxPeptideExpiry, "signal", currentAgent.getXLoc(), currentAgent.getYLoc());
                        peptides.add(np);
                        grid.addPeptide(np);
                    }
                }
            }
            else {
                                                                                                                            //System.out.println("... it died");
                grid.at(currentAgent.getXLoc(),currentAgent.getYLoc()).removeAgent(currentAgent);
                a.remove();

            }
        }

        // Randomise the order of ArrayList<Peptide>
        Collections.shuffle(peptides,rgen);

        // Iterate through ArrayList<Peptide>, removing any peptides if it should expire, and diffusing it otherwise
        ListIterator<Peptide> t = peptides.listIterator();
        while (t.hasNext()){
            Peptide currentPeptide = t.next();


            //System.out.println("Checking if peptide at (" + currentPeptide.getXLoc() + "," + currentPeptide.getYLoc() + ") will expire");
            if (currentPeptide.getExtinctionRate() < rgen.nextDouble()){
                grid.at(currentPeptide.getXLoc(),currentPeptide.getYLoc()).removePeptide(currentPeptide);
                t.remove();
            }
            else{
                currentPeptide.diffuse(rgen, grid);
            }
        }

        values.put("Agents",String.valueOf(agents.size()));
        values.put("Peptides",String.valueOf(peptides.size()));
        values.put("Fitness",String.valueOf(calcFitness()));
    }

    // calculate the fitness of the grid relative to the target
    public double calcFitness(){ //throws Exception {
        double diff = 0.0;

            /*
            for (int i = 0; i < gridHeight; i++) {
                for (int j = 0; j < gridWidth; j++) {
                    int gridValue = grid.at(i, j).getFitValue();
                    diff = diff + (target[i][j] - gridValue) * (target[i][j] - gridValue);
                }
            }
            */
            for (int i = 0; i < gridHeight; i++) {
                for (int j = 0; j < gridWidth; j++) {
                    int gridValue = grid.at(i, j).getFitValue();
                    if (gridValue != target[i][j])
                        diff = diff + 2;
                    else
                        diff = diff - 1;
                }
            }

        diff = diff / startingFit;
        return Math.sqrt(diff);
    }

    public void mutate(){
        for (Agent a : agents) {
            a.mutate(rgen, this.maxSensitivity, this.maxMotility, this.maxPeptidesProduced);
        }
    }
    public HashMap<String,String> getValues(){
        return values;
    }


}

