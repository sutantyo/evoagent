package main;


import javax.swing.*;

/**
 * Created by daniels on 26/02/2016.
 */
public class Main {

    public static final String currentVersion = "v1.0.0";

    public static void main(String args[]) {

        if (args.length == 0) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Evo controller = new Evo("graphical");
                }
            });
        }
        else{
                if (args[0].equals("--no-gui") || args[0].equals("-n")) {
                    if (args.length < 15) {
                        System.out.println("insufficient number of parameters");
                        System.out.println("usage: java -jar evoagent.jar -n <seed> <");
                    }
                    else {
                        Evo controller = new Evo("cli");
                        String result = controller.createAndRunSimulation(
                                args[1],
                                Integer.parseInt(args[2]),  // no of trials
                                Integer.parseInt(args[3]),  // no of steps per trial
                                Integer.parseInt(args[4]),  // seed

                                Integer.parseInt(args[5]),  // no of Agents
                                Integer.parseInt(args[6]),  // motility
                                Integer.parseInt(args[7]),  // sensitivity
                                Integer.parseInt(args[8]),  // colours
                                Integer.parseInt(args[9]),  // lifespan
                                Double.parseDouble(args[10]),// survival rate
                                Double.parseDouble(args[11]), //reproduction rate
                                Integer.parseInt(args[12]),   //peptides produced

                                Integer.parseInt(args[13]),  // starting peptides
                                Integer.parseInt(args[14]),  // diffusion rate
                                Double.parseDouble(args[15])  // peptide expiry
                        );
                        System.out.println(result);
                    }
                }
                else if (args[0].equals("--help") || args[0].equals("-h")) {
                    System.out.println(args[0]);
                }
                else if (args[0].equals("--version") || args[0].equals("-v")) {
                    System.out.println("EvoAgent " + currentVersion);
                }
                else {
                    System.out.println("Unknown options: " + args[0]);
                    System.out.println("usage: java -jar evoagent.jar [--no-gui <arguments>...] [--help] [--version]");
                }
            }
        }
}

