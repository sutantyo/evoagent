#!/bin/bash

echo No of trials, Steps per trial, Seed, No of Agents, Max Motility, Max Sensitivity, Max Colours, Max Lifespan, \
		 Survival Rate, Reproduction Rate, Peptides Produced, Starting Peptides, Max Diffusion Rate, Expiry Rate, Fitness

TRIALS=1
STEPS=1000
COLOURS=3
FILENAME="/Users/daniels/Dropbox/uni/biology/evoagent/out/artifacts/evoagent_jar/evoagent.jar -n "
IMAGE="frenchflag_60_40.pgm"

SEED_START=100
SEED_INC=1
SEED_END=150

AGENT_START=50
AGENT_INC=1
AGENT_END=50

MOT_START=1
MOT_INC=1
MOT_END=1

SENS_START=1
SENS_INC=1
SENS_END=1

LIFESPAN_START=1
LIFESPAN_INC=1
LIFESPAN_END=1

SURV_START=0.4
SURV_INC=0.1
SURV_END=0.4

REPROD_START=0.4
REPROD_INC=0.1
REPROD_END=0.4


for seed in `seq $SEED_START $SEED_INC $SEED_END`
do
	for agent in `seq $AGENT_START $AGENT_INC $AGENT_END`
	do
		for mot in `seq $MOT_START $MOT_INC $MOT_END`
		do
			for sens in `seq $SENS_START $SENS_INC $SENS_END`
			do
				for lifespan in `seq $LIFESPAN_START $LIFESPAN_INC $LIFESPAN_END`
				do
					for surv in `seq $SURV_START $SURV_INC $SURV_END`
					do
						for reprod in `seq $REPROD_START $REPROD_INC $REPROD_END`
						do
							java -jar $FILENAME $IMAGE $TRIALS $STEPS $seed $agent $mot $sens $COLOURS $lifespan $surv $reprod 1 1 1 1.0 
						done
					done
				done
			done
		done
	done
done

